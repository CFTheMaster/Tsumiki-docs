You can support the project on patreon: <https://patreon.com/computerfreaker> or paypal: <https://paypal.me/computerfreaker>

## Table Of Contents
- [Help](#help)
- [Administration](#administration)
- [Color](#color)
- [CustomReactions](#customreactions)
- [Gambling](#gambling)
- [Games](#games)
- [Music](#music)
- [NSFW](#nsfw)
- [Permissions](#permissions)
- [Pokemon](#pokemon)
- [Searches](#searches)
- [Utility](#utility)
- [Xp](#xp)


### Administration  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.delmsgoncmd` | Toggles the automatic deletion of the user's successful command message to prevent chat flood. You can use it either as a server toggle, channel whitelist, or channel blacklist, as channel option has 3 settings: Enable (always do it on this channel), Disable (never do it on this channel), and Inherit (respect server setting). Use `list` parameter to see the current states. **Requires Administrator server permission.** | `tsu.delmsgoncmd` or `tsu.delmsgoncmd channel enable` or `tsu.delmsgoncmd channel inherit` or `tsu.delmsgoncmd list`
`tsu.setrole` `tsu.sr` | Sets a role for a given user. **Requires ManageRoles server permission.** | `tsu.sr @User Guest`
`tsu.removerole` `tsu.rr` | Removes a role from a given user. **Requires ManageRoles server permission.** | `tsu.rr @User Admin`
`tsu.renamerole` `tsu.renr` | Renames a role. The role you are renaming must be lower than bot's highest role. **Requires ManageRoles server permission.** | `tsu.renr "First role" SecondRole`
`tsu.removeallroles` `tsu.rar` | Removes all roles from a mentioned user. **Requires ManageRoles server permission.** | `tsu.rar @User`
`tsu.createrole` `tsu.cr` | Creates a role with a given name. **Requires ManageRoles server permission.** | `tsu.cr Awesome Role`
`tsu.rolehoist` `tsu.rh` | Toggles whether this role is displayed in the sidebar or not. **Requires ManageRoles server permission.** | `tsu.rh Guests` or `tsu.rh "Space Wizards"`
`tsu.rolecolor` `tsu.roleclr` | Set a role's color to the hex or 0-255 rgb color value provided. **Requires ManageRoles server permission.** | `tsu.roleclr Admin 255 200 100` or `tsu.roleclr Admin ffba55`
`tsu.deafen` `tsu.deaf` | Deafens mentioned user or users. **Requires DeafenMembers server permission.** | `tsu.deaf "@Someguy"` or `tsu.deaf "@Someguy" "@Someguy"`
`tsu.undeafen` `tsu.undef` | Undeafens mentioned user or users. **Requires DeafenMembers server permission.** | `tsu.undef "@Someguy"` or `tsu.undef "@Someguy" "@Someguy"`
`tsu.delvoichanl` `tsu.dvch` | Deletes a voice channel with a given name. **Requires ManageChannels server permission.** | `tsu.dvch VoiceChannelName`
`tsu.creatvoichanl` `tsu.cvch` | Creates a new voice channel with a given name. **Requires ManageChannels server permission.** | `tsu.cvch VoiceChannelName`
`tsu.deltxtchanl` `tsu.dtch` | Deletes a text channel with a given name. **Requires ManageChannels server permission.** | `tsu.dtch TextChannelName`
`tsu.creatxtchanl` `tsu.ctch` | Creates a new text channel with a given name. **Requires ManageChannels server permission.** | `tsu.ctch TextChannelName`
`tsu.settopic` `tsu.st` | Sets a topic on the current channel. **Requires ManageChannels server permission.** | `tsu.st My new topic`
`tsu.setchanlname` `tsu.schn` | Changes the name of the current channel. **Requires ManageChannels server permission.** | `tsu.schn NewName`
`tsu.mentionrole` `tsu.menro` | Mentions every person from the provided role or roles (separated by a ',') on this server. **Requires MentionEveryone server permission.** | `tsu.menro RoleName`
`tsu.donators` | List of the lovely people who donated to keep this project alive.  | `tsu.donators`
`tsu.donadd` | Add a donator to the database. **Bot Owner Only** | `tsu.donadd Donate Amount`
`tsu.autoassignrole` `tsu.aar` | Automaticaly assigns a specified role to every user who joins the server. Provide no arguments to disable. **Requires ManageRoles server permission.** | `tsu.aar` or `tsu.aar RoleName`
`tsu.execsql` | Executes an sql command and returns the number of affected rows. Dangerous. **Bot Owner Only** | `tsu.execsql UPDATE Currency SET Amount=Amount+1234`
`tsu.deletewaifus` | Deletes everything from WaifuUpdates and WaifuInfo tables. **Bot Owner Only** | `tsu.deletewaifus`
`tsu.deletecurrency` | Deletes everything from Currency and CurrencyTransactions. **Bot Owner Only** | `tsu.deletecurrency`
`tsu.deleteplaylists` | Deletes everything from MusicPlaylists. **Bot Owner Only** | `tsu.deleteplaylists`
`tsu.deleteexp` | deleteexp **Bot Owner Only** | `deleteexp`
`tsu.gvc` | Toggles game voice channel feature in the voice channel you're currently in. Users who join the game voice channel will get automatically redirected to the voice channel with the name of their current game, if it exists. Can't move users to channels that the bot has no connect permission for. One per server. **Requires Administrator server permission.** | `tsu.gvc`
`tsu.languageset` `tsu.langset` | Sets this server's response language. If bot's response strings have been translated to that language, bot will use that language in this server. Reset by using `default` as the locale name. Provide no arguments to see currently set language.  | `tsu.langset de-DE ` or `tsu.langset default`
`tsu.langsetdefault` `tsu.langsetd` | Sets the bot's default response language. All servers which use a default locale will use this one. Setting to `default` will use the host's current culture. Provide no arguments to see currently set language.  | `tsu.langsetd en-US` or `tsu.langsetd default`
`tsu.languageslist` `tsu.langli` | List of languages for which translation (or part of it) exist atm.  | `tsu.langli`
`tsu.logserver` | Enables or Disables ALL log events. If enabled, all log events will log to this channel. **Requires Administrator server permission.** **Bot Owner Only** | `tsu.logserver enable` or `tsu.logserver disable`
`tsu.logignore` | Toggles whether the `.logserver` command ignores this channel. Useful if you have hidden admin channel and public log channel. **Requires Administrator server permission.** **Bot Owner Only** | `tsu.logignore`
`tsu.logevents` | Shows a list of all events you can subscribe to with `tsu.log` **Requires Administrator server permission.** **Bot Owner Only** | `tsu.logevents`
`tsu.log` | Toggles logging event. Disables it if it is active anywhere on the server. Enables if it isn't active. Use `tsu.logevents` to see a list of all events you can subscribe to. **Requires Administrator server permission.** **Bot Owner Only** | `tsu.log userpresence` or `tsu.log userbanned`
`tsu.setmuterole` | Sets a name of the role which will be assigned to people who should be muted. Default is Tsumiki-mute. **Requires ManageRoles server permission.** | `tsu.setmuterole Silenced`
`tsu.mute` | Mutes a mentioned user both from speaking and chatting. You can also specify time in minutes (up to 1440) for how long the user should be muted. **Requires ManageRoles server permission.** **Requires MuteMembers server permission.** | `tsu.mute @Someone` or `tsu.mute 30 @Someone`
`tsu.unmute` | Unmutes a mentioned user previously muted with `tsu.mute` command. **Requires ManageRoles server permission.** **Requires MuteMembers server permission.** | `tsu.unmute @Someone`
`tsu.chatmute` | Prevents a mentioned user from chatting in text channels. **Requires ManageRoles server permission.** | `tsu.chatmute @Someone`
`tsu.chatunmute` | Removes a mute role previously set on a mentioned user with `tsu.chatmute` which prevented him from chatting in text channels. **Requires ManageRoles server permission.** | `tsu.chatunmute @Someone`
`tsu.voicemute` | Prevents a mentioned user from speaking in voice channels. **Requires MuteMembers server permission.** | `tsu.voicemute @Someone`
`tsu.voiceunmute` | Gives a previously voice-muted user a permission to speak. **Requires MuteMembers server permission.** | `tsu.voiceunmute @Someguy`
`tsu.rotateplaying` `tsu.ropl` | Toggles rotation of playing status of the dynamic strings you previously specified. **Bot Owner Only** | `tsu.ropl`
`tsu.addplaying` `tsu.adpl` | Adds a specified string to the list of playing strings to rotate. You have to pick either 'Playing', 'Watching' or 'ListeningTo' as the first parameter. Supported placeholders: `%servers%`, `%users%`, `%playing%`, `%queued%`, `%time%`, `%shardid%`, `%shardcount%`, `%shardguilds%`. **Bot Owner Only** | `tsu.adpl Playing with you` or `tsu.adpl Watching you sleep`
`tsu.listplaying` `tsu.lipl` | Lists all playing statuses with their corresponding number. **Bot Owner Only** | `tsu.lipl`
`tsu.removeplaying` `tsu.rmpl` `tsu.repl` | Removes a playing string on a given number. **Bot Owner Only** | `tsu.rmpl`
`tsu.prefix` | Sets this server's prefix for all bot commands. Provide no arguments to see the current server prefix.  | `tsu.prefix +`
`tsu.defprefix` | Sets bot's default prefix for all bot commands. Provide no arguments to see the current default prefix. This will not change this server's current prefix. **Bot Owner Only** | `tsu.defprefix +`
`tsu.antiraid` | Sets an anti-raid protection on the server. First argument is number of people which will trigger the protection. Second one is a time interval in which that number of people needs to join in order to trigger the protection, and third argument is punishment for those people (Kick, Ban, Mute) **Requires Administrator server permission.** | `tsu.antiraid 5 20 Kick`
`tsu.antispam` | Stops people from repeating same message X times in a row. You can specify to either mute, kick or ban the offenders. If you're using mute, you can add a number of seconds at the end to use a timed mute. Max message count is 10. **Requires Administrator server permission.** | `tsu.antispam 3 Mute` or `tsu.antispam 4 Kick` or `tsu.antispam 6 Ban`
`tsu.antispamignore` | Toggles whether antispam ignores current channel. Antispam must be enabled. **Requires Administrator server permission.** | `tsu.antispamignore`
`tsu.antilist` `tsu.antilst` | Shows currently enabled protection features.  | `tsu.antilist`
`tsu.prune` `tsu.clear` | `tsu.prune` removes all Tsumiki's messages in the last 100 messages. `tsu.prune X` removes last `X` number of messages from the channel (up to 100). `tsu.prune @Someone` removes all Someone's messages in the last 100 messages. `tsu.prune @Someone X` removes last `X` number of 'Someone's' messages in the channel.  | `tsu.prune` or `tsu.prune 5` or `tsu.prune @Someone` or `tsu.prune @Someone X`
`tsu.slowmode` | Toggles slowmode. Disable by specifying no parameters. To enable, specify a number of messages each user can send, and an interval in seconds. For example 1 message every 5 seconds. **Requires ManageMessages server permission.** | `tsu.slowmode 1 5` or `tsu.slowmode`
`tsu.slowmodewl` | Ignores a role or a user from the slowmode feature. **Requires ManageMessages server permission.** | `tsu.slowmodewl SomeRole` or `tsu.slowmodewl AdminDude`
`tsu.adsarm` | Toggles the automatic deletion of confirmations for `tsu.iam` and `tsu.iamn` commands. **Requires ManageMessages server permission.** | `tsu.adsarm`
`tsu.asar` | Adds a role to the list of self-assignable roles. You can also specify a group. If 'Exclusive self-assignable roles' feature is enabled, users will be able to pick one role per group. **Requires ManageRoles server permission.** | `tsu.asar Gamer` or `tsu.asar 1 Alliance` or `tsu.asar 1 Horde`
`tsu.rsar` | Removes a specified role from the list of self-assignable roles. **Requires ManageRoles server permission.** | `tsu.rsar`
`tsu.lsar` | Lists all self-assignable roles.  | `tsu.lsar`
`tsu.togglexclsar` `tsu.tesar` | Toggles whether the self-assigned roles are exclusive. While enabled, users can only have one self-assignable role per group. **Requires ManageRoles server permission.** | `tsu.tesar`
`tsu.rolelevelreq` `tsu.rlr` | Set a level requirement on a self-assignable role. **Requires ManageRoles server permission.** | `tsu.rlr 5 SomeRole`
`tsu.iam` | Adds a role to you that you choose. Role must be on a list of self-assignable roles.  | `tsu.iam Gamer`
`tsu.iamnot` `tsu.iamn` | Removes a specified role from you. Role must be on a list of self-assignable roles.  | `tsu.iamn Gamer`
`tsu.scadd` | Adds a command to the list of commands which will be executed automatically in the current channel, in the order they were added in, by the bot when it startups up. **Bot Owner Only** | `tsu.scadd .stats`
`tsu.sclist` | Lists all startup commands in the order they will be executed in. **Bot Owner Only** | `tsu.sclist`
`tsu.wait` | Used only as a startup command. Waits a certain number of miliseconds before continuing the execution of the following startup commands. **Bot Owner Only** | `tsu.wait 3000`
`tsu.scrm` | Removes a startup command with the provided command text. **Bot Owner Only** | `tsu.scrm .stats`
`tsu.scclr` | Removes all startup commands. **Bot Owner Only** | `tsu.scclr`
`tsu.fwmsgs` | Toggles forwarding of non-command messages sent to bot's DM to the bot owners **Bot Owner Only** | `tsu.fwmsgs`
`tsu.fwtoall` | Toggles whether messages will be forwarded to all bot owners or only to the first one specified in the credentials.json file **Bot Owner Only** | `tsu.fwtoall`
`tsu.shardstats` | Stats for shards. Paginated with 25 shards per page.  | `tsu.shardstats` or `tsu.shardstats 2`
`tsu.restartshard` | Try (re)connecting a shard with a certain shardid when it dies. No one knows will it work. Keep an eye on the console for errors. **Bot Owner Only** | `tsu.restartshard 2`
`tsu.leave` | Makes Tsumiki leave the server. Either server name or server ID is required. **Bot Owner Only** | `tsu.leave 123123123331`
`tsu.die` | Shuts the bot down. **Bot Owner Only** | `tsu.die`
`tsu.restart` | Restarts the bot. Might not work. **Bot Owner Only** | `tsu.restart`
`tsu.setname` `tsu.newnm` | Gives the bot a new name. **Bot Owner Only** | `tsu.newnm BotName`
`tsu.setnick` | Changes the nickname of the bot on this server. You can also target other users to change their nickname. **Requires ManageNicknames server permission.** | `tsu.setnick BotNickname` or `tsu.setnick @SomeUser New Nickname`
`tsu.setstatus` | Sets the bot's status. (Online/Idle/Dnd/Invisible) **Bot Owner Only** | `tsu.setstatus Idle`
`tsu.setavatar` `tsu.setav` | Sets a new avatar image for the TsumikiBot. Argument is a direct link to an image. **Bot Owner Only** | `tsu.setav http://i.imgur.com/xTG3a1I.jpg`
`tsu.setgame` | Sets the bots game status to either Playing, ListeningTo, or Watching. **Bot Owner Only** | `tsu.setgame Playing with snakes.` or `tsu.setgame Watching anime.` or `tsu.setgame ListeningTo music.`
`tsu.setstream` | Sets the bots stream. First argument is the twitch link, second argument is stream name. **Bot Owner Only** | `tsu.setstream TWITCHLINK Hello`
`tsu.send` | Sends a message to someone on a different server through the bot.  Separate server and channel/user ids with `|` and prefix the channel id with `c:` and the user id with `u:`. **Bot Owner Only** | `tsu.send serverid|c:channelid message` or `tsu.send serverid|u:userid message`
`tsu.imagesreload` | Reloads images bot is using. Safe to use even when bot is being used heavily. **Bot Owner Only** | `tsu.imagesreload`
`tsu.botconfigreload` | Reloads bot configuration in case you made changes to the BotConfig table either with .execsql or manually in the .db file. **Bot Owner Only** | `tsu.botconfigreload`
`tsu.greetdel` `tsu.grdel` | Sets the time it takes (in seconds) for greet messages to be auto-deleted. Set it to 0 to disable automatic deletion. **Requires ManageServer server permission.** | `tsu.greetdel 0` or `tsu.greetdel 30`
`tsu.greet` | Toggles anouncements on the current channel when someone joins the server. **Requires ManageServer server permission.** | `tsu.greet`
`tsu.greetmsg` | Sets a new join announcement message which will be shown in the server's channel. Type `%user%` if you want to mention the new member. Using it with no message will show the current greet message. You can use embed json from <http://nadekobot.me/embedbuilder/> instead of a regular text, if you want the message to be embedded. **Requires ManageServer server permission.** | `tsu.greetmsg Welcome, %user%.`
`tsu.greetdm` | Toggles whether the greet messages will be sent in a DM (This is separate from greet - you can have both, any or neither enabled). **Requires ManageServer server permission.** | `tsu.greetdm`
`tsu.greetdmmsg` | Sets a new join announcement message which will be sent to the user who joined. Type `%user%` if you want to mention the new member. Using it with no message will show the current DM greet message. You can use embed json from <http://nadekobot.me/embedbuilder/> instead of a regular text, if you want the message to be embedded. **Requires ManageServer server permission.** | `tsu.greetdmmsg Welcome to the server, %user%`
`tsu.bye` | Toggles anouncements on the current channel when someone leaves the server. **Requires ManageServer server permission.** | `tsu.bye`
`tsu.byemsg` | Sets a new leave announcement message. Type `%user%` if you want to show the name the user who left. Type `%id%` to show id. Using this command with no message will show the current bye message. You can use embed json from <http://nadekobot.me/embedbuilder/> instead of a regular text, if you want the message to be embedded. **Requires ManageServer server permission.** | `tsu.byemsg %user% has left.`
`tsu.byedel` | Sets the time it takes (in seconds) for bye messages to be auto-deleted. Set it to `0` to disable automatic deletion. **Requires ManageServer server permission.** | `tsu.byedel 0` or `tsu.byedel 30`
`tsu.timezones` | Lists all timezones available on the system to be used with `tsu.timezone`.  | `tsu.timezones`
`tsu.timezone` | Sets this guilds timezone. This affects bot's time output in this server (logs, etc..)  | `tsu.timezone` or `tsu.timezone GMT Standard Time`
`tsu.warn` | Warns a user. **Requires BanMembers server permission.** | `tsu.warn @b1nzy Very rude person`
`tsu.warnlog` | See a list of warnings of a certain user. **Requires BanMembers server permission.** | `tsu.warnlog @b1nzy`
`tsu.warnlogall` | See a list of all warnings on the server. 15 users per page. **Requires BanMembers server permission.** | `tsu.warnlogall` or `tsu.warnlogall 2`
`tsu.warnclear` `tsu.warnc` | Clears all warnings from a certain user. **Requires BanMembers server permission.** | `tsu.warnclear @PoorDude`
`tsu.warnpunish` `tsu.warnp` | Sets a punishment for a certain number of warnings. Provide no punishment to remove. **Requires BanMembers server permission.** | `tsu.warnpunish 5 Ban` or `tsu.warnpunish 3`
`tsu.warnpunishlist` `tsu.warnpl` | Lists punishments for warnings.  | `tsu.warnpunishlist`
`tsu.ban` `tsu.b` | Bans a user by ID or name with an optional message. **Requires BanMembers server permission.** | `tsu.b "@some Guy" Your behaviour is toxic.`
`tsu.unban` | Unbans a user with the provided user#discrim or id. **Requires BanMembers server permission.** | `tsu.unban kwoth#1234` or `tsu.unban 123123123`
`tsu.softban` `tsu.sb` | Bans and then unbans a user by ID or name with an optional message. **Requires KickMembers server permission.** **Requires ManageMessages server permission.** | `tsu.sb "@some Guy" Your behaviour is toxic.`
`tsu.kick` `tsu.k` | Kicks a mentioned user. **Requires KickMembers server permission.** | `tsu.k "@some Guy" Your behaviour is toxic.`
`tsu.masskill` | Specify a new-line separated list of `userid reason`. You can use Username#discrim instead of UserId. Specified users will be banned from the current server, blacklisted from the bot, and have all of their flowers taken away. **Requires BanMembers server permission.** **Bot Owner Only** | `tsu.masskill BadPerson#1234 Toxic person`
`tsu.vcrole` | Sets or resets a role which will be given to users who join the voice channel you're in when you run this command. Provide no role name to disable. You must be in a voice channel to run this command. **Requires ManageRoles server permission.** **Requires ManageChannels server permission.** | `tsu.vcrole SomeRole` or `tsu.vcrole`
`tsu.vcrolelist` | Shows a list of currently set voice channel roles.  | `tsu.vcrolelist`
`tsu.voice+text` `tsu.v+t` | Creates a text channel for each voice channel only users in that voice channel can see. If you are server owner, keep in mind you will see them all the time regardless. **Requires ManageRoles server permission.** **Requires ManageChannels server permission.** | `tsu.v+t`
`tsu.cleanvplust` `tsu.cv+t` | Deletes all text channels ending in `-voice` for which voicechannels are not found. Use at your own risk. **Requires ManageChannels server permission.** **Requires ManageRoles server permission.** | `tsu.cleanv+t`

###### [Back to ToC](#table-of-contents)

### Color  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.rainbow` | Changes the visible color of user. Color in hex. "Something" for a random color. Blank to remove color.  | `tsu.rainbow` or `tsu.rainbow #123456` or `tsu.rainbow something`
`tsu.setusercolor` | Changes the color of specified user. Color in hex, blank to remove **Requires ManageMessages server permission.** | `tsu.setusercolor @person` or `tsu.setusercolor @person #123456`

###### [Back to ToC](#table-of-contents)

### CustomReactions  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.addcustreact` `tsu.acr` | Add a custom reaction with a trigger and a response. Running this command in server requires the Administration permission. Running this command in DM is Bot Owner only and adds a new global custom reaction. Guide here: <http://tsumiki.readthedocs.io/en/latest/Custom%20Reactions/>  | `tsu.acr "hello" Hi there %user%`
`tsu.editcustreact` `tsu.ecr` | Edits the custom reaction's response given its ID.  | `tsu.ecr 123 I'm a magical girl`
`tsu.listcustreact` `tsu.lcr` | Lists global or server custom reactions (20 commands per page). Running the command in DM will list global custom reactions, while running it in server will list that server's custom reactions. Specifying `all` argument instead of the number will DM you a text file with a list of all custom reactions.  | `tsu.lcr 1` or `tsu.lcr all`
`tsu.listcustreactg` `tsu.lcrg` | Lists global or server custom reactions (20 commands per page) grouped by trigger, and show a number of responses for each. Running the command in DM will list global custom reactions, while running it in server will list that server's custom reactions.  | `tsu.lcrg 1`
`tsu.showcustreact` `tsu.scr` | Shows a custom reaction's response on a given ID.  | `tsu.scr 1`
`tsu.delcustreact` `tsu.dcr` | Deletes a custom reaction on a specific index. If ran in DM, it is bot owner only and deletes a global custom reaction. If ran in a server, it requires Administration privileges and removes server custom reaction.  | `tsu.dcr 5`
`tsu.crca` | Toggles whether the custom reaction will trigger if the triggering message contains the keyword (instead of only starting with it).  | `tsu.crca 44`
`tsu.crdm` | Toggles whether the response message of the custom reaction will be sent as a direct message.  | `tsu.crdm 44`
`tsu.crad` | Toggles whether the message triggering the custom reaction will be automatically deleted.  | `tsu.crad 59`
`tsu.crstatsclear` | Resets the counters on `tsu.crstats`. You can specify a trigger to clear stats only for that trigger. **Bot Owner Only** | `tsu.crstatsclear` or `tsu.crstatsclear rng`
`tsu.crstats` | Shows a list of custom reactions and the number of times they have been executed. Paginated with 10 per page. Use `tsu.crstatsclear` to reset the counters.  | `tsu.crstats` or `tsu.crstats 3`

###### [Back to ToC](#table-of-contents)

### Gambling  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.timely` | Use to claim your 'timely' currency. Bot owner has to specify the amount and the period on how often you can claim your currency.  | `tsu.timely`
`tsu.timelyreset` | Resets all user timeouts on `tsu.timely` command. **Bot Owner Only** | `tsu.timelyreset`
`tsu.timelyset` | Sets the 'timely' currency allowance amount for users. Second argument is period in hours, default is 24 hours. **Bot Owner Only** | `tsu.timelyset 100` or `tsu.timelyset 50 12`
`tsu.raffle` | Prints a name and ID of a random online user from the server, or from the online user in the specified role.  | `tsu.raffle` or `tsu.raffle RoleName`
`tsu.raffleany` | Prints a name and ID of a random user from the server, or from the specified role.  | `tsu.raffleany` or `tsu.raffleany  RoleName`
`tsu.$` `tsu.currency` `tsu.$$` `tsu.$$$` `tsu.cash` `tsu.cur` | Check how much currency a person has. (Defaults to yourself)  | `tsu.$` or `tsu.$ @SomeGuy`
`tsu.give` | Give someone a certain amount of currency.  | `tsu.give 1 @SomeGuy`
`tsu.award` | Awards someone a certain amount of currency.  You can also specify a role name to award currency to all users in a role. **Bot Owner Only** | `tsu.award 100 @person` or `tsu.award 5 Role Of Gamblers`
`tsu.take` | Takes a certain amount of currency from someone. **Bot Owner Only** | `tsu.take 1 @SomeGuy`
`tsu.rollduel` | Challenge someone to a roll duel by specifying the amount and the user you wish to challenge as the parameters. To accept the challenge, just specify the name of the user who challenged you, without the amount.  | `tsu.rollduel 50 @SomeGuy` or `tsu.rollduel @Challenger`
`tsu.betroll` `tsu.br` | Bets a certain amount of currency and rolls a dice. Rolling over 66 yields x2 of your currency, over 90 - x4 and 100 x10.  | `tsu.br 5`
`tsu.leaderboard` `tsu.lb` | Displays the bot's currency leaderboard.  | `tsu.lb`
`tsu.race` | Starts a new animal race.  | `tsu.race`
`tsu.joinrace` `tsu.jr` | Joins a new race. You can specify an amount of currency for betting (optional). You will get YourBet*(participants-1) back if you win.  | `tsu.jr` or `tsu.jr 5`
`tsu.startevent` | Starts one of the events seen on public Tsumiki. `reaction` and `sneakygamestatus` are the only 2 available now. **Bot Owner Only** | `tsu.startevent reaction`
`tsu.rafflecur` | Starts or joins a currency raffle with a specified amount. Users who join the raffle will lose the amount of currency specified and add it to the pot. After 30 seconds, random winner will be selected who will receive the whole pot. There is also a `mixed` mode in which the users will be able to join the game with any amount of currency, and have their chances be proportional to the amount they've bet.  | `tsu.rafflecur 20` or `tsu.rafflecur mixed 15`
`tsu.roll` | Rolls 0-100. If you supply a number `X` it rolls up to 30 normal dice. If you split 2 numbers with letter `d` (`xdy`) it will roll `X` dice from 1 to `y`. `Y` can be a letter 'F' if you want to roll fate dice instead of dnd.  | `tsu.roll` or `tsu.roll 7` or `tsu.roll 3d5` or `tsu.roll 5dF`
`tsu.rolluo` | Rolls `X` normal dice (up to 30) unordered. If you split 2 numbers with letter `d` (`xdy`) it will roll `X` dice from 1 to `y`.  | `tsu.rolluo` or `tsu.rolluo 7` or `tsu.rolluo 3d5`
`tsu.nroll` | Rolls in a given range. If you specify just one number instead of the range, it will role from 0 to that number.  | `tsu.nroll 5` or `tsu.nroll 5-15`
`tsu.draw` | Draws a card from this server's deck. You can draw up to 10 cards by supplying a number of cards to draw.  | `tsu.draw` or `tsu.draw 5`
`tsu.drawnew` | Draws a card from the NEW deck of cards. You can draw up to 10 cards by supplying a number of cards to draw.  | `tsu.drawnew` or `tsu.drawnew 5`
`tsu.deckshuffle` `tsu.dsh` | Reshuffles all cards back into the deck.  | `tsu.dsh`
`tsu.flip` | Flips coin(s) - heads or tails, and shows an image.  | `tsu.flip` or `tsu.flip 3`
`tsu.betflip` `tsu.bf` | Bet to guess will the result be heads or tails. Guessing awards you 1.95x the currency you've bet (rounded up). Multiplier can be changed by the bot owner.  | `tsu.bf 5 heads` or `tsu.bf 3 t`
`tsu.shop` | Lists this server's administrators' shop. Paginated.  | `tsu.shop` or `tsu.shop 2`
`tsu.buy` | Buys an item from the shop on a given index. If buying items, make sure that the bot can DM you.  | `tsu.buy 2`
`tsu.shopadd` | Adds an item to the shop by specifying type price and name. Available types are role and list. **Requires Administrator server permission.** | `tsu.shopadd role 1000 Rich`
`tsu.shoplistadd` | Adds an item to the list of items for sale in the shop entry given the index. You usually want to run this command in the secret channel, so that the unique items are not leaked. **Requires Administrator server permission.** | `tsu.shoplistadd 1 Uni-que-Steam-Key`
`tsu.shoprem` `tsu.shoprm` | Removes an item from the shop by its ID. **Requires Administrator server permission.** | `tsu.shoprm 1`
`tsu.slotstats` | Shows the total stats of the slot command for this bot's session. **Bot Owner Only** | `tsu.slotstats`
`tsu.slottest` | Tests to see how much slots payout for X number of plays. **Bot Owner Only** | `tsu.slottest 1000`
`tsu.slot` | Play Tsumiki slots. Max bet is 9999. 1.5 second cooldown per user.  | `tsu.slot 5`
`tsu.waifureset` | Resets your waifu stats, except current waifus.  | `tsu.waifureset`
`tsu.claimwaifu` `tsu.claim` | Claim a waifu for yourself by spending currency.  You must spend at least 10% more than her current value unless she set `tsu.affinity` towards you.  | `tsu.claim 50 @Himesama`
`tsu.waifutransfer` | Transfer the ownership of one of your waifus to another user. You must pay 10% of your waifu's value.  | `tsu.waifutransfer @ExWaifu @NewOwner`
`tsu.divorce` | Releases your claim on a specific waifu. You will get some of the money you've spent back unless that waifu has an affinity towards you. 6 hours cooldown.  | `tsu.divorce @CheatingSloot`
`tsu.affinity` | Sets your affinity towards someone you want to be claimed by. Setting affinity will reduce their `tsu.claim` on you by 20%. You can leave second argument empty to clear your affinity. 30 minutes cooldown.  | `tsu.affinity @MyHusband` or `tsu.affinity`
`tsu.waifus` `tsu.waifulb` | Shows top 9 waifus. You can specify another page to show other waifus.  | `tsu.waifus` or `tsu.waifulb 3`
`tsu.waifuinfo` `tsu.waifustats` | Shows waifu stats for a target person. Defaults to you if no user is provided.  | `tsu.waifuinfo @MyCrush` or `tsu.waifuinfo`
`tsu.waifugift` `tsu.gift` `tsu.gifts` | Gift an item to someone. This will increase their waifu value by 50% of the gifted item's value if they don't have affinity set towards you, or 100% if they do. Provide no arguments to see a list of items that you can gift.  | `tsu.gifts` or `tsu.gift Rose @Himesama`
`tsu.wheeloffortune` `tsu.wheel` | Bets a certain amount of currency on the wheel of fortune. Wheel can stop on one of many different multipliers. Won amount is rounded down to the nearest whole number.  | `tsu.wheel 10`

###### [Back to ToC](#table-of-contents)

### Games  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.choose` | Chooses a thing from a list of things  | `tsu.choose Get up;Sleep;Sleep more`
`tsu.8ball` | Ask the 8ball a yes/no question.  | `tsu.8ball Is b1nzy a nice guy?`
`tsu.rps` | Play a game of Rocket-Paperclip-Scissors with Tsumiki.  | `tsu.rps scissors`
`tsu.rategirl` | Use the universal hot-crazy wife zone matrix to determine the girl's worth. It is everything young men need to know about women. At any moment in time, any woman you have previously located on this chart can vanish from that location and appear anywhere else on the chart.  | `tsu.rategirl @SomeGurl`
`tsu.linux` | Prints a customizable Linux interjection  | `tsu.linux Spyware Windows`
`tsu.leet` | Converts a text to leetspeak with 6 (1-6) severity levels  | `tsu.leet 3 Hello`
`tsu.acrophobia` `tsu.acro` | Starts an Acrophobia game.  | `tsu.acro` or `tsu.acro -s 30`
`tsu.cleverbot` | Toggles cleverbot session. When enabled, the bot will reply to messages starting with bot mention in the server. Custom reactions starting with %mention% won't work if cleverbot is enabled. **Requires ManageMessages server permission.** | `tsu.cleverbot`
`tsu.connect4` `tsu.con4` | Creates or joins an existing connect4 game. 2 players are required for the game. Objective of the game is to get 4 of your pieces next to each other in a vertical, horizontal or diagonal line.  | `tsu.connect4`
`tsu.hangmanlist` | Shows a list of hangman term types.  | `tsu.hangmanlist`
`tsu.hangman` | Starts a game of hangman in the channel. Use `tsu.hangmanlist` to see a list of available term types. Defaults to 'all'.  | `tsu.hangman` or `tsu.hangman movies`
`tsu.hangmanstop` | Stops the active hangman game on this channel if it exists.  | `tsu.hangmanstop`
`tsu.nunchi` | Creates or joins an existing nunchi game. Users have to count up by 1 from the starting number shown by the bot. If someone makes a mistake (types an incorrent number, or repeats the same number) they are out of the game and a new round starts without them.  Minimum 3 users required.  | `tsu.nunchi`
`tsu.pick` | Picks the currency planted in this channel. 60 seconds cooldown.  | `tsu.pick`
`tsu.plant` | Spend an amount of currency to plant it in this channel. Default is 1. (If bot is restarted or crashes, the currency will be lost)  | `tsu.plant` or `tsu.plant 5`
`tsu.gencurrency` `tsu.gc` | Toggles currency generation on this channel. Every posted message will have chance to spawn currency. Chance is specified by the Bot Owner. (default is 2%) **Requires ManageMessages server permission.** | `tsu.gc`
`tsu.poll` `tsu.ppoll` | Creates a public poll which requires users to type a number of the voting option in the channel command is ran in. **Requires ManageMessages server permission.** | `tsu.ppoll Question?;Answer1;Answ 2;A_3`
`tsu.pollstats` | Shows the poll results without stopping the poll on this server. **Requires ManageMessages server permission.** | `tsu.pollstats`
`tsu.pollend` | Stops active poll on this server and prints the results in this channel. **Requires ManageMessages server permission.** | `tsu.pollend`
`tsu.typestart` | Starts a typing contest.  | `tsu.typestart`
`tsu.typestop` | Stops a typing contest on the current channel.  | `tsu.typestop`
`tsu.typeadd` | Adds a new article to the typing contest. **Bot Owner Only** | `tsu.typeadd wordswords`
`tsu.typelist` | Lists added typing articles with their IDs. 15 per page.  | `tsu.typelist` or `tsu.typelist 3`
`tsu.typedel` | Deletes a typing article given the ID. **Bot Owner Only** | `tsu.typedel 3`
`tsu.tictactoe` `tsu.ttt` | Starts a game of tic tac toe. Another user must run the command in the same channel in order to accept the challenge. Use numbers 1-9 to play.  | `tsu.ttt`
`tsu.trivia` `tsu.t` | Starts a game of trivia. You can add `nohint` to prevent hints. First player to get to 10 points wins by default. You can specify a different number. 30 seconds per question.  | `tsu.t` or `tsu.t --timeout 5 -p -w 3 -q 10`
`tsu.tl` | Shows a current trivia leaderboard.  | `tsu.tl`
`tsu.tq` | Quits current trivia after current question.  | `tsu.tq`

###### [Back to ToC](#table-of-contents)

### Help  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.modules` `tsu.mdls` | Lists all bot modules.  | `tsu.modules`
`tsu.commands` `tsu.cmds` | List all of the bot's commands from a certain module. You can either specify the full name or only the first few letters of the module name.  | `tsu.commands Administration` or `tsu.cmds Admin`
`tsu.help` `tsu.h` | Either shows a help for a single command, or DMs you help link if no arguments are specified.  | `tsu.h tsu.cmds` or `tsu.h`
`tsu.hgit` | Generates the commandlist.md file. **Bot Owner Only** | `tsu.hgit`
`tsu.donate` | Instructions for helping the project financially.  | `tsu.donate`

###### [Back to ToC](#table-of-contents)

### Music  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.play` `tsu.start` | If no arguments are specified, acts as `tsu.next 1` command. If you specify a song number, it will jump to that song. If you specify a search query, acts as a `tsu.q` command  | `tsu.play` or `tsu.play 5` or `tsu.play Dream Of Venice`
`tsu.queue` `tsu.q` `tsu.yq` | Queue a song using keywords or a link. Bot will join your voice channel. **You must be in a voice channel**.  | `tsu.q Dream Of Venice`
`tsu.queuenext` `tsu.qn` | Works the same as `tsu.queue` command, except it enqueues the new song after the current one. **You must be in a voice channel**.  | `tsu.qn Dream Of Venice`
`tsu.queuesearch` `tsu.qs` `tsu.yqs` | Search for top 5 youtube song result using keywords, and type the index of the song to play that song. Bot will join your voice channel. **You must be in a voice channel**.  | `tsu.qs Dream Of Venice`
`tsu.listqueue` `tsu.lq` | Lists 10 currently queued songs per page. Default page is 1.  | `tsu.lq` or `tsu.lq 2`
`tsu.next` `tsu.n` | Goes to the next song in the queue. You have to be in the same voice channel as the bot. You can skip multiple songs, but in that case songs will not be requeued if tsu.rcs or tsu.rpl is enabled.  | `tsu.n` or `tsu.n 5`
`tsu.stop` `tsu.s` | Stops the music and preserves the current song index. Stays in the channel.  | `tsu.s`
`tsu.autodisconnect` `tsu.autodc` | Toggles whether the bot should disconnect from the voice channel once it's done playing all of the songs.  | `tsu.autodc`
`tsu.destroy` `tsu.d` | Completely stops the music and unbinds the bot from the channel. (may cause weird behaviour)  | `tsu.d`
`tsu.pause` `tsu.p` | Pauses or Unpauses the song.  | `tsu.p`
`tsu.volume` `tsu.vol` | Sets the music playback volume (0-100%)  | `tsu.vol 50`
`tsu.defvol` `tsu.dv` | Sets the default music volume when music playback is started (0-100). Persists through restarts.  | `tsu.dv 80`
`tsu.songremove` `tsu.srm` | Remove a song by its # in the queue, or 'all' to remove all songs from the queue and reset the song index.  | `tsu.srm 5`
`tsu.playlists` `tsu.pls` | Lists all playlists. Paginated, 20 per page. Default page is 0.  | `tsu.pls 1`
`tsu.deleteplaylist` `tsu.delpls` | Deletes a saved playlist using its id. Works only if you made it or if you are the bot owner.  | `tsu.delpls 5`
`tsu.save` | Saves a playlist under a certain name. Playlist name must be no longer than 20 characters and must not contain dashes.  | `tsu.save classical1`
`tsu.load` | Loads a saved playlist using its ID. Use `tsu.pls` to list all saved playlists and `tsu.save` to save new ones.  | `tsu.load 5`
`tsu.fairplay` `tsu.fp` | Toggles fairplay. While enabled, the bot will prioritize songs from users who didn't have their song recently played instead of the song's position in the queue.  | `tsu.fp`
`tsu.songautodelete` `tsu.sad` | Toggles whether the song should be automatically removed from the music queue when it finishes playing.  | `tsu.sad`
`tsu.soundcloudqueue` `tsu.sq` | Queue a soundcloud song using keywords. Bot will join your voice channel. **You must be in a voice channel**.  | `tsu.sq Dream Of Venice`
`tsu.soundcloudpl` `tsu.scpl` | Queue a Soundcloud playlist using a link.  | `tsu.scpl soundcloudseturl`
`tsu.nowplaying` `tsu.np` | Shows the song that the bot is currently playing.  | `tsu.np`
`tsu.shuffle` `tsu.sh` `tsu.plsh` | Shuffles the current playlist.  | `tsu.plsh`
`tsu.playlist` `tsu.pl` | Queues up to 500 songs from a youtube playlist specified by a link, or keywords.  | `tsu.pl <youtube_playlist_link>`
`tsu.radio` `tsu.ra` | Queues a radio stream from a link. It can be a direct mp3 radio stream, .m3u, .pls .asx or .xspf (Usage Video: <https://streamable.com/al54>)  | `tsu.ra radio link here`
`tsu.local` `tsu.lo` | Queues a local file by specifying a full path. **Bot Owner Only** | `tsu.lo C:/music/mysong.mp3`
`tsu.localplaylst` `tsu.lopl` | Queues all songs from a directory. **Bot Owner Only** | `tsu.lopl C:/music/classical`
`tsu.move` `tsu.mv` | Moves the bot to your voice channel. (works only if music is already playing)  | `tsu.mv`
`tsu.movesong` `tsu.ms` | Moves a song from one position to another.  | `tsu.ms 5>3`
`tsu.setmaxqueue` `tsu.smq` | Sets a maximum queue size. Supply 0 or no argument to have no limit.  | `tsu.smq 50` or `tsu.smq`
`tsu.setmaxplaytime` `tsu.smp` | Sets a maximum number of seconds (>14) a song can run before being skipped automatically. Set 0 to have no limit.  | `tsu.smp 0` or `tsu.smp 270`
`tsu.reptcursong` `tsu.rcs` | Toggles repeat of current song.  | `tsu.rcs`
`tsu.rpeatplaylst` `tsu.rpl` | Toggles repeat of all songs in the queue (every song that finishes is added to the end of the queue).  | `tsu.rpl`
`tsu.autoplay` `tsu.ap` | Toggles autoplay - When the song is finished, automatically queue a related Youtube song. (Works only for Youtube songs and when queue is empty)  | `tsu.ap`
`tsu.setmusicchannel` `tsu.smch` | Sets the current channel as the default music output channel. This will output playing, finished, paused and removed songs to that channel instead of the channel where the first song was queued in. **Requires ManageMessages server permission.** | `tsu.smch`
`tsu.unsetmusicchannel` `tsu.usmch` | Bot will output playing, finished, paused and removed songs to the channel where the first song was queued in. **Requires ManageMessages server permission.** | `tsu.smch`

###### [Back to ToC](#table-of-contents)

### NSFW  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.autohentai` | Posts a hentai every X seconds with a random tag from the provided tags. Use `|` to separate tags. 20 seconds minimum. Provide no arguments to disable. **Requires ManageMessages channel permission.** | `tsu.autohentai 30 yuri|tail|long_hair` or `tsu.autohentai`
`tsu.autoboobs` | Posts a boobs every X seconds. 20 seconds minimum. Provide no arguments to disable. **Requires ManageMessages channel permission.** | `tsu.autoboobs 30` or `tsu.autoboobs`
`tsu.autobutts` | Posts a butts every X seconds. 20 seconds minimum. Provide no arguments to disable. **Requires ManageMessages channel permission.** | `tsu.autobutts 30` or `tsu.autobutts`
`tsu.hentai` | Shows a hentai image from a random website (gelbooru or danbooru or konachan or atfbooru or yandere) with a given tag. Tag is optional but preferred. Only 1 tag allowed.  | `tsu.hentai yuri`
`tsu.hentaibomb` | Shows a total 5 images (from gelbooru, danbooru, konachan, yandere and atfbooru). Tag is optional but preferred.  | `tsu.hentaibomb yuri`
`tsu.yandere` | Shows a random image from yandere with a given tag. Tag is optional but preferred. (multiple tags are appended with +)  | `tsu.yandere tag1+tag2`
`tsu.konachan` | Shows a random hentai image from konachan with a given tag. Tag is optional but preferred.  | `tsu.konachan yuri`
`tsu.e621` | Shows a random hentai image from e621.net with a given tag. Tag is optional but preferred. Use spaces for multiple tags.  | `tsu.e621 yuri kissing`
`tsu.rule34` | Shows a random image from rule34.xx with a given tag. Tag is optional but preferred. (multiple tags are appended with +)  | `tsu.rule34 yuri+kissing`
`tsu.danbooru` | Shows a random hentai image from danbooru with a given tag. Tag is optional but preferred. (multiple tags are appended with +)  | `tsu.danbooru yuri+kissing`
`tsu.gelbooru` | Shows a random hentai image from gelbooru with a given tag. Tag is optional but preferred. (multiple tags are appended with +)  | `tsu.gelbooru yuri+kissing`
`tsu.boobs` | Real adult content.  | `tsu.boobs`
`tsu.butts` `tsu.ass` `tsu.butt` | Real adult content.  | `tsu.butts` or `tsu.ass`
`tsu.locallewds` | get some locallewds from my collection takes gifs, pngs and jpgs as image formats **Bot Owner Only** | `tsu.locallewds`
`tsu.nsfwtagbl` `tsu.nsfwtbl` | Toggles whether the tag is blacklisted or not in nsfw searches. Provide no parameters to see the list of blacklisted tags.  | `tsu.nsfwtbl poop`
`tsu.nsfwcc` | Clears nsfw cache. **Bot Owner Only** | `tsu.nsfwcc`

###### [Back to ToC](#table-of-contents)

### Permissions  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.verbose` `tsu.v` | Sets whether to show when a command/module is blocked.  | `tsu.verbose true`
`tsu.permrole` `tsu.pr` | Sets a role which can change permissions. Supply no parameters to see the current one. Default is 'Tsumiki'.  | `tsu.pr role`
`tsu.listperms` `tsu.lp` | Lists whole permission chain with their indexes. You can specify an optional page number if there are a lot of permissions.  | `tsu.lp` or `tsu.lp 3`
`tsu.removeperm` `tsu.rp` | Removes a permission from a given position in the Permissions list.  | `tsu.rp 1`
`tsu.moveperm` `tsu.mp` | Moves permission from one position to another in the Permissions list.  | `tsu.mp 2 4`
`tsu.srvrcmd` `tsu.sc` | Sets a command's permission at the server level.  | `tsu.sc "command name" disable`
`tsu.srvrmdl` `tsu.sm` | Sets a module's permission at the server level.  | `tsu.sm ModuleName enable`
`tsu.usrcmd` `tsu.uc` | Sets a command's permission at the user level.  | `tsu.uc "command name" enable SomeUsername`
`tsu.usrmdl` `tsu.um` | Sets a module's permission at the user level.  | `tsu.um ModuleName enable SomeUsername`
`tsu.rolecmd` `tsu.rc` | Sets a command's permission at the role level.  | `tsu.rc "command name" disable MyRole`
`tsu.rolemdl` `tsu.rm` | Sets a module's permission at the role level.  | `tsu.rm ModuleName enable MyRole`
`tsu.chnlcmd` `tsu.cc` | Sets a command's permission at the channel level.  | `tsu.cc "command name" enable SomeChannel`
`tsu.chnlmdl` `tsu.cm` | Sets a module's permission at the channel level.  | `tsu.cm ModuleName enable SomeChannel`
`tsu.allchnlmdls` `tsu.acm` | Enable or disable all modules in a specified channel.  | `tsu.acm enable #SomeChannel`
`tsu.allrolemdls` `tsu.arm` | Enable or disable all modules for a specific role.  | `tsu.arm [enable/disable] MyRole`
`tsu.allusrmdls` `tsu.aum` | Enable or disable all modules for a specific user.  | `tsu.aum enable @someone`
`tsu.allsrvrmdls` `tsu.asm` | Enable or disable all modules for your server.  | `tsu.asm [enable/disable]`
`tsu.ubl` | Either [add]s or [rem]oves a user specified by a Mention or an ID from a blacklist. **Bot Owner Only** | `tsu.ubl add @SomeUser` or `tsu.ubl rem 12312312313`
`tsu.cbl` | Either [add]s or [rem]oves a channel specified by an ID from a blacklist. **Bot Owner Only** | `tsu.cbl rem 12312312312`
`tsu.sbl` | Either [add]s or [rem]oves a server specified by a Name or an ID from a blacklist. **Bot Owner Only** | `tsu.sbl add 12312321312` or `tsu.sbl rem SomeTrashServer`
`tsu.cmdcooldown` `tsu.cmdcd` | Sets a cooldown per user for a command. Set it to 0 to remove the cooldown.  | `tsu.cmdcd "some cmd" 5`
`tsu.allcmdcooldowns` `tsu.acmdcds` | Shows a list of all commands and their respective cooldowns.  | `tsu.acmdcds`
`tsu.srvrfilterinv` `tsu.sfi` | Toggles automatic deletion of invites posted in the server. Does not affect the Bot Owner.  | `tsu.sfi`
`tsu.chnlfilterinv` `tsu.cfi` | Toggles automatic deletion of invites posted in the channel. Does not negate the `tsu.srvrfilterinv` enabled setting. Does not affect the Bot Owner.  | `tsu.cfi`
`tsu.srvrfilterwords` `tsu.sfw` | Toggles automatic deletion of messages containing filtered words on the server. Does not affect the Bot Owner.  | `tsu.sfw`
`tsu.chnlfilterwords` `tsu.cfw` | Toggles automatic deletion of messages containing filtered words on the channel. Does not negate the `tsu.srvrfilterwords` enabled setting. Does not affect the Bot Owner.  | `tsu.cfw`
`tsu.fw` | Adds or removes (if it exists) a word from the list of filtered words. Use`tsu.sfw` or `tsu.cfw` to toggle filtering.  | `tsu.fw poop`
`tsu.lstfilterwords` `tsu.lfw` | Shows a list of filtered words.  | `tsu.lfw`
`tsu.listglobalperms` `tsu.lgp` | Lists global permissions set by the bot owner. **Bot Owner Only** | `tsu.lgp`
`tsu.globalmodule` `tsu.gmod` | Toggles whether a module can be used on any server. **Bot Owner Only** | `tsu.gmod nsfw`
`tsu.globalcommand` `tsu.gcmd` | Toggles whether a command can be used on any server. **Bot Owner Only** | `tsu.gcmd .stats`
`tsu.resetperms` | Resets the bot's permissions module on this server to the default value. **Requires Administrator server permission.** | `tsu.resetperms`
`tsu.resetglobalperms` | Resets global permissions set by bot owner. **Bot Owner Only** | `tsu.resetglobalperms`

###### [Back to ToC](#table-of-contents)

### Pokemon  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.attack` | Attacks a target with the given move. Use `tsu.movelist` to see a list of moves your type can use.  | `tsu.attack "vine whip" @someguy`
`tsu.movelist` `tsu.ml` | Lists the moves you are able to use  | `tsu.ml`
`tsu.heal` | Heals someone. Revives those who fainted. Costs one Currency.   | `tsu.heal @someone`
`tsu.type` | Get the poketype of the target.  | `tsu.type @someone`
`tsu.settype` | Set your poketype. Costs one Currency. Provide no arguments to see a list of available types.  | `tsu.settype fire` or `tsu.settype`

###### [Back to ToC](#table-of-contents)

### Searches  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.lolban` | Shows top banned champions ordered by ban rate.  | `tsu.lolban`
`tsu.crypto` `tsu.c` | Shows basic stats about a cryptocurrency from coinmarketcap.com. You can use either a name or an abbreviation of the currency.  | `tsu.c btc` or `tsu.c bitcoin`
`tsu.rip` | rip  | `rip`
`tsu.say` | Bot will send the message you typed in this channel. Supports embeds. **Requires ManageMessages server permission.** | `tsu.say hi`
`tsu.weather` `tsu.we` | Shows weather data for a specified city. You can also specify a country after a comma.  | `tsu.we Moscow, RU`
`tsu.time` | Shows the current time and timezone in the specified location.  | `tsu.time London, UK`
`tsu.youtube` `tsu.yt` | Searches youtubes and shows the first result  | `tsu.yt query`
`tsu.imdb` `tsu.omdb` | Queries omdb for movies or series, show first result.  | `tsu.imdb Batman vs Superman`
`tsu.randomcat` `tsu.meow` | Shows a random cat image.  | `tsu.meow`
`tsu.randomdog` `tsu.woof` | Shows a random dog image.  | `tsu.woof`
`tsu.image` `tsu.img` | Pulls the first image found using a search parameter. Use `tsu.rimg` for different results.  | `tsu.img cute kitten`
`tsu.randomimage` `tsu.rimg` | Pulls a random image using a search parameter.  | `tsu.rimg cute kitten`
`tsu.lmgtfy` | Google something for an idiot.  | `tsu.lmgtfy query`
`tsu.shorten` | Attempts to shorten an URL, if it fails, returns the input URL.  | `tsu.shorten https://google.com`
`tsu.google` `tsu.g` | Get a Google search link for some terms.  | `tsu.google query`
`tsu.magicthegathering` `tsu.mtg` | Searches for a Magic The Gathering card.  | `tsu.magicthegathering about face` or `tsu.mtg about face`
`tsu.hearthstone` `tsu.hs` | Searches for a Hearthstone card and shows its image. Takes a while to complete.  | `tsu.hs Ysera`
`tsu.yodify` `tsu.yoda` | Translates your normal sentences into Yoda styled sentences!  | `tsu.yoda my feelings hurt`
`tsu.urbandict` `tsu.ud` | Searches Urban Dictionary for a word.  | `tsu.ud Pineapple`
`tsu.define` `tsu.def` | Finds a definition of a word.  | `tsu.def heresy`
`tsu.#` | Searches Tagdef.com for a hashtag.  | `tsu.# ff`
`tsu.catfact` | Shows a random catfact from <http://catfacts-api.appspot.com/api/facts>  | `tsu.catfact`
`tsu.revav` | Returns a Google reverse image search for someone's avatar.  | `tsu.revav @SomeGuy`
`tsu.revimg` | Returns a Google reverse image search for an image from a link.  | `tsu.revimg Image link`
`tsu.safebooru` | Shows a random image from safebooru with a given tag. Tag is optional but preferred. (multiple tags are appended with +)  | `tsu.safebooru yuri+kissing`
`tsu.wikipedia` `tsu.wiki` | Gives you back a wikipedia link  | `tsu.wiki query`
`tsu.color` | Shows you what color corresponds to that hex.  | `tsu.color 00ff00`
`tsu.videocall` | Creates a private <http://www.appear.in> video call link for you and other mentioned people. The link is sent to mentioned people via a private message.  | `tsu.videocall "@the First" "@Xyz"`
`tsu.avatar` `tsu.av` | Shows a mentioned person's avatar.  | `tsu.av @SomeGuy`
`tsu.wikia` | Gives you back a wikia link  | `tsu.wikia mtg Vigilance` or `tsu.wikia mlp Dashy`
`tsu.novel` | Searches for a novel on `http://novelupdates.com/`. You have to provide an exact name.  | `tsu.novel the nine cauldrons`
`tsu.mal` | Shows basic info from a MyAnimeList profile.  | `tsu.mal straysocks`
`tsu.anime` `tsu.ani` `tsu.aq` | Queries anilist for an anime and shows the first result.  | `tsu.ani aquarion evol`
`tsu.manga` `tsu.mang` `tsu.mq` | Queries anilist for a manga and shows the first result.  | `tsu.mq Shingeki no kyojin`
`tsu.feed` `tsu.feedadd` | Subscribes to a feed. Bot will post an update up to once every 10 seconds. You can have up to 10 feeds on one server. All feeds must have unique URLs. **Requires ManageMessages server permission.** | `tsu.feed https://www.rt.com/rss/`
`tsu.feedremove` `tsu.feedrm` `tsu.feeddel` | Stops tracking a feed on the given index. Use `tsu.feeds` command to see a list of feeds and their indexes. **Requires ManageMessages server permission.** | `tsu.feedremove 3`
`tsu.feeds` `tsu.feedlist` | Shows the list of feeds you've subscribed to on this server. **Requires ManageMessages server permission.** | `tsu.feeds`
`tsu.yomama` `tsu.ym` | Shows a random joke from <http://api.yomomma.info/>  | `tsu.ym`
`tsu.randjoke` `tsu.rj` | Shows a random joke from <http://tambal.azurewebsites.net/joke/random>  | `tsu.rj`
`tsu.chucknorris` `tsu.cn` | Shows a random Chuck Norris joke from <http://api.icndb.com/jokes/random/>  | `tsu.cn`
`tsu.wowjoke` | Get one of computerfreakers penultimate WoW jokes.  | `tsu.wowjoke`
`tsu.magicitem` `tsu.mi` | Shows a random magic item from <https://1d4chan.org/wiki/List_of_/tg/%27s_magic_items>  | `tsu.mi`
`tsu.memelist` | Pulls a list of memes you can use with `tsu.memegen` from http://memegen.link/templates/  | `tsu.memelist`
`tsu.memegen` | Generates a meme from memelist with top and bottom text.  | `tsu.memegen biw "gets iced coffee" "in the winter"`
`tsu.osu` | Shows osu stats for a player.  | `tsu.osu Name` or `tsu.osu Name taiko`
`tsu.osub` | Shows information about an osu beatmap.  | `tsu.osub https://osu.ppy.sh/s/127712`
`tsu.osu5` | Displays a user's top 5 plays.  | `tsu.osu5 Name`
`tsu.overwatch` `tsu.ow` | Show's basic stats on a player (competitive rank, playtime, level etc) Region codes are: `eu` `us` `cn` `kr`  | `tsu.ow us Battletag#1337` or `tsu.overwatch eu Battletag#2016`
`tsu.pathofexile` `tsu.poe` | Searches characters for a given Path of Exile account. May specify league name to filter results.  | `tsu.poe "Zizaran"`
`tsu.pathofexileleagues` `tsu.poel` | Returns a list of the main Path of Exile leagues.  | `tsu.poel`
`tsu.pathofexilecurrency` `tsu.poec` | Returns the chaos equivalent of a given currency or exchange rate between two currencies.  | `tsu.poec Standard "Mirror of Kalandra"`
`tsu.pathofexileitem` `tsu.poei` | Searches for a Path of Exile item from the Path of Exile GamePedia.  | `tsu.poei "Quill Rain"`
`tsu.placelist` | Shows the list of available tags for the `tsu.place` command.  | `tsu.placelist`
`tsu.place` | Shows a placeholder image of a given tag. Use `tsu.placelist` to see all available tags. You can specify the width and height of the image as the last two optional arguments.  | `tsu.place Cage` or `tsu.place steven 500 400`
`tsu.pokemon` `tsu.poke` | Searches for a pokemon.  | `tsu.poke Sylveon`
`tsu.pokemonability` `tsu.pokeab` | Searches for a pokemon ability.  | `tsu.pokeab overgrow`
`tsu.smashcast` `tsu.hb` | Notifies this channel when the specified user starts streaming. **Requires ManageMessages server permission.** | `tsu.smashcast SomeStreamer`
`tsu.twitch` `tsu.tw` | Notifies this channel when the specified user starts streaming. **Requires ManageMessages server permission.** | `tsu.twitch SomeStreamer`
`tsu.picarto` `tsu.pa` | Notifies this channel when the specified user starts streaming. **Requires ManageMessages server permission.** | `tsu.picarto SomeStreamer`
`tsu.mixer` `tsu.bm` | Notifies this channel when the specified user starts streaming. **Requires ManageMessages server permission.** | `tsu.mixer SomeStreamer`
`tsu.liststreams` `tsu.ls` | Lists all streams you are following on this server.  | `tsu.ls`
`tsu.removestream` `tsu.rms` | Removes notifications of a certain streamer from a certain platform on this channel. **Requires ManageMessages server permission.** | `tsu.rms Twitch SomeGuy` or `tsu.rms mixer SomeOtherGuy`
`tsu.checkstream` `tsu.cs` | Checks if a user is online on a certain streaming platform.  | `tsu.cs twitch MyFavStreamer`
`tsu.translate` `tsu.trans` | Translates from>to text. From the given language to the destination language.  | `tsu.trans en>fr Hello`
`tsu.autotrans` `tsu.at` | Starts automatic translation of all messages by users who set their `tsu.atl` in this channel. You can set "del" argument to automatically delete all translated user messages. **Requires Administrator server permission.** **Bot Owner Only** | `tsu.at` or `tsu.at del`
`tsu.autotranslang` `tsu.atl` | Sets your source and target language to be used with `tsu.at`. Specify no arguments to remove previously set value.  | `tsu.atl en>fr`
`tsu.translangs` | Lists the valid languages for translation.  | `tsu.translangs`
`tsu.xkcd` | Shows a XKCD comic. No arguments will retrieve random one. Number argument will retrieve a specific comic, and "latest" will get the latest one.  | `tsu.xkcd` or `tsu.xkcd 1400` or `tsu.xkcd latest`

###### [Back to ToC](#table-of-contents)

### Utility  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.togethertube` `tsu.totube` | Creates a new room on <https://togethertube.com> and shows the link in the chat.  | `tsu.totube`
`tsu.whosplaying` `tsu.whpl` | Shows a list of users who are playing the specified game.  | `tsu.whpl Overwatch`
`tsu.inrole` | Lists every person from the specified role on this server. You can use role ID, role name.  | `tsu.inrole Some Role`
`tsu.checkperms` | Checks yours or bot's user-specific permissions on this channel.  | `tsu.checkperms me` or `tsu.checkperms bot`
`tsu.userid` `tsu.uid` | Shows user ID.  | `tsu.uid` or `tsu.uid @SomeGuy`
`tsu.channelid` `tsu.cid` | Shows current channel ID.  | `tsu.cid`
`tsu.serverid` `tsu.sid` | Shows current server ID.  | `tsu.sid`
`tsu.roles` | List roles on this server or roles of a user if specified. Paginated, 20 roles per page.  | `tsu.roles 2` or `tsu.roles @Someone`
`tsu.channeltopic` `tsu.ct` | Sends current channel's topic as a message.  | `tsu.ct`
`tsu.createinvite` `tsu.crinv` | Creates a new invite which has infinite max uses and never expires. **Requires CreateInstantInvite channel permission.** | `tsu.crinv`
`tsu.stats` | Shows some basic stats for Tsumiki.  | `tsu.stats`
`tsu.showemojis` `tsu.se` | Shows a name and a link to every SPECIAL emoji in the message.  | `tsu.se A message full of SPECIAL emojis`
`tsu.listservers` | Lists servers the bot is on with some basic info. 15 per page. **Bot Owner Only** | `tsu.listservers 3`
`tsu.savechat` | Saves a number of messages to a text file and sends it to you. **Bot Owner Only** | `tsu.savechat 150`
`tsu.ping` | Ping the bot to see if there are latency issues.  | `tsu.ping`
`tsu.botconfigedit` `tsu.bce` | Sets one of available bot config settings to a specified value. Use the command without any parameters to get a list of available settings. **Bot Owner Only** | `tsu.bce CurrencyName b1nzy` or `tsu.bce`
`tsu.calculate` `tsu.calc` | Evaluate a mathematical expression.  | `tsu.calc 1+1`
`tsu.calcops` | Shows all available operations in the `tsu.calc` command  | `tsu.calcops`
`tsu.alias` `tsu.cmdmap` | Create a custom alias for a certain Tsumiki command. Provide no alias to remove the existing one. **Requires Administrator server permission.** | `tsu.alias allin $bf 100 h` or `tsu.alias "linux thingy" >loonix Spyware Windows`
`tsu.aliaslist` `tsu.cmdmaplist` `tsu.aliases` | Shows the list of currently set aliases. Paginated.  | `tsu.aliaslist` or `tsu.aliaslist 3`
`tsu.serverinfo` `tsu.sinfo` | Shows info about the server the bot is on. If no server is supplied, it defaults to current one.  | `tsu.sinfo Some Server`
`tsu.channelinfo` `tsu.cinfo` | Shows info about the channel. If no channel is supplied, it defaults to current one.  | `tsu.cinfo #some-channel`
`tsu.userinfo` `tsu.uinfo` | Shows info about the user. If no user is supplied, it defaults a user running the command.  | `tsu.uinfo @SomeUser`
`tsu.activity` | Checks for spammers. **Bot Owner Only** | `tsu.activity`
`tsu.parewrel` | Forces the update of the list of patrons who are eligible for the reward.  | `tsu.parewrel`
`tsu.clparew` `tsu.claparew` | Claim patreon rewards. If you're subscribed to bot owner's patreon you can use this command to claim your rewards - assuming bot owner did setup has their patreon key.  | `tsu.clparew`
`tsu.listquotes` `tsu.liqu` | Lists all quotes on the server ordered alphabetically or by ID. 15 Per page.  | `tsu.liqu 3` or `tsu.liqu 3 id`
`tsu...` | Shows a random quote with a specified name.  | `tsu... abc`
`tsu.qsearch` | Shows a random quote for a keyword that contains any text specified in the search.  | `tsu.qsearch keyword text`
`tsu.quoteid` `tsu.qid` | Displays the quote with the specified ID number. Quote ID numbers can be found by typing `.liqu [num]` where `[num]` is a number of a page which contains 15 quotes.  | `tsu.qid 123456`
`tsu..` | Adds a new quote with the specified name and message.  | `tsu.. sayhi Hi`
`tsu.quotedel` `tsu.qdel` | Deletes a quote with the specified ID. You have to be either server Administrator or the creator of the quote to delete it.  | `tsu.qdel 123456`
`tsu.delallq` `tsu.daq` | Deletes all quotes on a specified keyword. **Requires Administrator server permission.** | `tsu.delallq kek`
`tsu.remind` | Sends a message to you or a channel after certain amount of time. First argument is `me`/`here`/'channelname'. Second argument is time in a descending order (mo>w>d>h>m) example: 1w5d3h10m. Third argument is a (multiword) message.  | `tsu.remind me 1d5h Do something` or `tsu.remind #general 1m Start now!`
`tsu.remindtemplate` | Sets message for when the remind is triggered.  Available placeholders are `%user%` - user who ran the command, `%message%` - Message specified in the remind, `%target%` - target channel of the remind. **Bot Owner Only** | `tsu.remindtemplate %user%, do %message%!`
`tsu.repeatinvoke` `tsu.repinv` | Immediately shows the repeat message on a certain index and restarts its timer. **Requires ManageMessages server permission.** | `tsu.repinv 1`
`tsu.repeatremove` `tsu.reprm` | Removes a repeating message on a specified index. Use `tsu.repeatlist` to see indexes. **Requires ManageMessages server permission.** | `tsu.reprm 2`
`tsu.repeat` | Repeat a message every `X` minutes in the current channel. You can instead specify time of day for the message to be repeated at daily (make sure you've set your server's timezone). You can have up to 5 repeating messages on the server in total. **Requires ManageMessages server permission.** | `tsu.repeat 5 Hello there` or `tsu.repeat 17:30 tea time`
`tsu.repeatlist` `tsu.replst` | Shows currently repeating messages and their indexes. **Requires ManageMessages server permission.** | `tsu.repeatlist`
`tsu.streamrole` | Sets a role which is monitored for streamers (FromRole), and a role to add if a user from 'FromRole' is streaming (AddRole). When a user from 'FromRole' starts streaming, they will receive an 'AddRole'. Provide no arguments to disable **Requires ManageRoles server permission.** | `tsu.streamrole "Eligible Streamers" "Featured Streams"`
`tsu.streamrolekw` `tsu.srkw` | Sets keyword which is required in the stream's title in order for the streamrole to apply. Provide no keyword in order to reset. **Requires ManageRoles server permission.** | `tsu.srkw` or `tsu.srkw PUBG`
`tsu.streamrolebl` `tsu.srbl` | Adds or removes a blacklisted user. Blacklisted users will never receive the stream role. **Requires ManageRoles server permission.** | `tsu.srbl add @b1nzy#1234` or `tsu.srbl rem @b1nzy#1234`
`tsu.streamrolewl` `tsu.srwl` | Adds or removes a whitelisted user. Whitelisted users will receive the stream role even if they don't have the specified keyword in their stream title. **Requires ManageRoles server permission.** | `tsu.srwl add @b1nzy#1234` or `tsu.srwl rem @b1nzy#1234`
`tsu.convertlist` | List of the convertible dimensions and currencies.  | `tsu.convertlist`
`tsu.convert` | Convert quantities. Use `tsu.convertlist` to see supported dimensions and currencies.  | `tsu.convert m km 1000`
`tsu.verboseerror` `tsu.ve` | Toggles whether the bot should print command errors when a command is incorrectly used. **Requires ManageMessages server permission.** | `tsu.ve`

###### [Back to ToC](#table-of-contents)

### Xp  
Command and aliases | Description | Usage
----------------|--------------|-------
`tsu.experience` `tsu.xp` | Shows your xp stats. Specify the user to show that user's stats instead.  | `tsu.xp`
`tsu.xplvluprewards` `tsu.xprews` `tsu.xpcrs` `tsu.xprrs` `tsu.xprolerewards` `tsu.xpcurrewards` | Shows currently set level up rewards.  | `tsu.xprews`
`tsu.xprolereward` `tsu.xprr` | Sets a role reward on a specified level. Provide no role name in order to remove the role reward. **Requires ManageRoles server permission.** | `tsu.xprr 3 Social`
`tsu.xpcurreward` `tsu.xpcr` | Sets a currency reward on a specified level. Provide no amount in order to remove the reward. **Bot Owner Only** | `tsu.xpcr 3 50`
`tsu.xpnotify` `tsu.xpn` | Sets how the bot should notify you when you get a `server` or `global` level. You can set `dm` (for the bot to send a direct message), `channel` (to get notified in the channel you sent the last message in) or `none` to disable.  | `tsu.xpn global dm` or `tsu.xpn server channel`
`tsu.xpexclude` `tsu.xpex` | Exclude a channel, role or current server from the xp system. **Requires Administrator server permission.** | `tsu.xpex Role Excluded-Role` or `tsu.xpex Server`
`tsu.xpexclusionlist` `tsu.xpexl` | Shows the roles and channels excluded from the XP system on this server, as well as whether the whole server is excluded.  | `tsu.xpexl`
`tsu.xpleaderboard` `tsu.xplb` | Shows current server's xp leaderboard.  | `tsu.xplb`
`tsu.xpgleaderboard` `tsu.xpglb` | Shows the global xp leaderboard.  | `tsu.xpglb`
`tsu.xpadd` | Adds xp to a user on the server. This does not affect their global ranking. You can use negative values. **Requires Administrator server permission.** | `tsu.xpadd 100 @b1nzy`
`tsu.clubtransfer` | Transfers the ownership of the club to another member of the club.  | `tsu.clubtransfer @b1nzy`
`tsu.clubadmin` | Assigns (or unassigns) staff role to the member of the club. Admins can ban, kick and accept applications.  | `tsu.clubadmin`
`tsu.clubcreate` | Creates a club. You must be at least level 5 and not be in the club already.  | `tsu.clubcreate b1nzy's friends`
`tsu.clubicon` | Sets the club icon.  | `tsu.clubicon https://i.imgur.com/htfDMfU.png`
`tsu.clubinfo` | Shows information about the club.  | `tsu.clubinfo b1nzy's friends#123`
`tsu.clubbans` | Shows the list of users who have banned from your club. Paginated. You must be club owner to use this command.  | `tsu.clubbans 2`
`tsu.clubapps` | Shows the list of users who have applied to your club. Paginated. You must be club owner to use this command.  | `tsu.clubapps 2`
`tsu.clubapply` | Apply to join a club. You must meet that club's minimum level requirement, and not be on its ban list.  | `tsu.clubapply b1nzy's friends#123`
`tsu.clubaccept` | Accept a user who applied to your club.  | `tsu.clubaccept b1nzy#1337`
`tsu.clubleave` | Leaves the club you're currently in.  | `tsu.clubleave`
`tsu.clubkick` | Kicks the user from the club. You must be the club owner. They will be able to apply again.  | `tsu.clubkick b1nzy#1337`
`tsu.clubban` | Bans the user from the club. You must be the club owner. They will not be able to apply again.  | `tsu.clubban b1nzy#1337`
`tsu.clubunban` | Unbans the previously banned user from the club. You must be the club owner.  | `tsu.clubunban b1nzy#1337`
`tsu.clublevelreq` | Sets the club required level to apply to join the club. You must be club owner. You can't set this number below 5.  | `tsu.clublevelreq 7`
`tsu.clubdisband` | Disbands the club you're the owner of. This action is irreversible.  | `tsu.clubdisband`
`tsu.clublb` | Shows club rankings on the specified page.  | `tsu.clublb 2`
