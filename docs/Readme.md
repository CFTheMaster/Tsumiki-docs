##Readme for Commands List

###Bot Owner Only

- *Bot Owner Only* commands refer to the commands only the **owner** of the bot can use.
- *Bot Owner Only* commands do **not** refer to the owner of the **server**, just the owner of the **bot**.
- *Owner of the bot* is a person who is **hosting** their own bot, and their **ID** is inside of **credentials.json** file.
- You are **not** the bot **owner** if you invited the bot using **Carbonitex** or other invitation links.

###Music on Tsumiki

- May lag from time to time

###TsumikiFlowers

- TsumikiFlowers is the **currency** of Tsumiki.
- TsumikiFlowers can be `tsu.pick`ed after Tsumiki plants a flower randomly after `.gc` has been enabled on a channel
- You can give TsumikiFlowers to other users, using the command `tsu.give X @person`.
- You can only give flowers you **own**.
- If you want to have **unlimited** number of flowers, you will have to **host** the bot.
- Commands `.award X @person` and `tsu.take X @person` can only be used by the *bot owner*.
- If you `tsu.plant` the flower, flower will be avaliable for everyone to `tsu.pick` it. In that case you will **lose** the flower.

###Manage Permissions

**These permissions refer to the permissions you can set in Discord settings for individual users or roles.**
