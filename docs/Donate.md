##Donate to Tsumiki

If you want to help Tsumiki and Tsumiki's team by donating, you can do that in the two of the following ways:

###Patreon

You can donate over [Patreon][Patreon] and support the project.

[![img][img]](https://www.patreon.com/computerfreaker)


###PayPal

If you wish to donate over PayPal, you can send your donations to: [PayPal Link](https://www.paypal.me/computerfreaker)

[Patreon]: https://www.patreon.com/computerfreaker
[img]: http://www.mister-and-me.com/wp-content/plugins/patron-button-and-widgets-by-codebard/images/patreon-medium-button.png
