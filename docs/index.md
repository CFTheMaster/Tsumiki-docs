#TsumikiBot Documentation

To invite Tsumiki to your server, click on the image bellow:

[![img][img]](https://discordapp.com/oauth2/authorize?client_id=230232604597682177&scope=bot&permissions=66186303)

In case you need any help, hop on the [TsumikiBot Server][TsumikiBot Server], where we can provide support.

##Content
- [About](about.md)
- Commands
	- [Readme](Readme.md)
	- [Commands List](Commands List.md)
- Features Explained
	- [Permissions System](Permissions System.md)
	- [Custom Reactions](Custom Reactions.md)
	- [Placeholders](Placeholders.md)
- [Frequently Asked Questions](Frequently Asked Questions.md)
- [Donate](Donate.md)

[img]: https://cdn.discordapp.com/attachments/202743183774318593/210580315381563392/discord.png
[TsumikiBot Server]: https://discord.gg/rMVju6a