#Frequently Asked Questions


###Question 1: How do I get Tsumiki to join my server?
----
**Answer:** Simply send Tsumiki a Direct Message with `.h` and follow the link. **Only People with the Manage Server permission can add the bot to the server**

###Question 2: I want to change permissions, but it isn't working!
----
**Answer:** You must have the `tsu.permrole` (by default this is the `Tsumiki` role, for more details on permissions check [here](http://Tsumikibot.readthedocs.io/en/latest/Permissions%20System/ "Permissions"). If you have a role called `Tsumiki` but can't assign it it's probably the Bot Role so, just create a **New Role** called `Tsumiki` and assign that to yourself instead.)

###Question 3: I want to disable NSFW on my server.
----
**Answer:** To disable the NSFW Module for your server type, `tsu.sm NSFW disable`. If this does not work refer to Question 2.

###Question 4: How do I get TsumikiFlowers/Currency?
----
**Answer:** You can get TsumikiFlowers by picking them up after they have been generated with `tsu.gc`, which you can then either plant (give away to a channel so that someone can pick it), or gamble with for potentinal profit with `tsu.betflip`, `tsu.betroll` and `tsu.jr`

###Question 5: I have an issue/bug/suggestion, where do I put it so it gets noticed?
-----------
**Answer:** You can come to my guild and put your issue/bug/suggestion in #support_features, [discord invite link](https://discord.gg/rMVju6a)

If your problem or suggestion is not there, feel free to request/notify me about it.

###Question 6: How do I use this command?
--------
**Answer:** You can see the description and usage of certain commands by using `tsu.h command` **i.e** `tsu.h tsu.sm`. 

The whole list of commands can be found [here](http://Tsumiki.readthedocs.io/en/latest/Commands%20List/ "Command List")

###Question 7: Music isn't working?
----
**Answer:** Music is laggy on Tsumiki due to my laptop being old and not the fastes. 

###Question 8: My music is still not working/very laggy?
----
**Answer:** Try changing your discord [location][1], if this doesn't work be sure you have enabled the correct permissions for Tsumiki and rebooted since installing FFMPEG.
[1]: https://support.discordapp.com/hc/en-us/articles/216661717.how-do-I-change-my-Voice-Server-Region-

###Question 9: The tsu.greet and tsu.bye commands doesn't work, but everything else is!
-----
**Answer:** Set a greeting message by using `tsu.greetmsg YourMessageHere` and a bye-message by using `tsu.byemsg YourMessageHere`. Don't forget that `tsu.greet` and `tsu.bye` only apply to users joining a server, not coming online/offline.

###Question 10: I've broken permissions and am stuck, can I reset permissions?
----------
**Answer:** Yes, there is a way, in one easy command! Just run `tsu.resetperms` and all the permissions you've set through **Permissions Module** will reset.
