[![Discord](https://discordapp.com/api/guilds/138303776170835969/widget.png)](https://discord.gg/rMVju6a)[![pipeline status](https://gitlab.com/CFTheMaster/TsumikiBot/badges/master/pipeline.svg)](https://gitlab.com/CFTheMaster/TsumikiBot/commits/master)[![Documentation Status](http://readthedocs.org/projects/tsumiki/badge/?version=latest)](http://tsumiki.readthedocs.io/en/latest/?badge=latest)
## For Updates, Help and Guidelines

| [![twitter](https://cdn.discordapp.com/attachments/155726317222887425/252192520094613504/twiter_banner.JPG)](https://twitter.com/nintendoDSgek) | [![discord](https://cdn.discordapp.com/attachments/266240393639755778/281920766490968064/discord.png)](https://discord.gg/rMVju6a) | [![invite my bot](https://cdn.discordapp.com/avatars/230232604597682177/90f32903fd2d5e53b537b0eec407d531.png)](https://discordapp.com/oauth2/authorize?client_id=230232604597682177&scope=bot&permissions=66186303)
| --- | --- | --- |
| **Follow me on Twitter.** | **Join my Discord server for help.** | **add my bot** |
